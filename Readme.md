# GEK10.3 Datamanagement "Distributed Data Structures"

## Vergleich

### Atomix

#### Architektur

* Cluster auf Basis des Raft Protokolls    
Das Raft Protokoll wurde erstellt, um das Probleme des Konsens zwischen mehreren verteilten Systemen zu lösen. Unter Konsens versteht man, dass multiple Server sich bei einer Entscheidung einigen können. Wenn bei Raft sich alle Server auf beispielsweise einen Wert einigen, dann haben alle Nodes in einem Raft-Cluster diesen Wert gespeichert. Bei Raft gibt es kein fixes Master-Slave Pattern, es gibt immer wieder "Wahlen", wo ein Master für kurze Zeit gewählt wird, der für gewisse Zeit einige Berechtigungen besitzt.
  
* Verteilung von Aufgaben mittels EventService   
Mithilfe des ClusterEventServices kann eine Nachricht an eine Node gesendet werden, dabei wird aber nicht spezifiziert, welche Node genau die Nachricht erhält. Das Cluster entscheidet intern, welche Node ausgewählt wird. Die Auswahl der Node erfolgt im Round-Robin-Verfahren. Dadurch kann man Aufgaben von Clients auf die Nodes verteilen.

#### Sprachen

* Java und somit alle anderen Sprachen, die auf der JVM basieren (Kotlin, Scala, .. ).
* Es gibt einen zusätzlichen Go-Client, in der man in der Programmiersprache Golang mit den Cluster kommunizieren kann.
* Atomix bietet auch eine REST-Schnittstelle, die man mit anderen Programmiersprachen aufrufen kann. Jedoch ist die Dokumentation dazu immer noch ein "Work in Progress".

#### Datenverteilung

* Atomix bietet mehrere APIs an, um Werte als "States" zwischen mehreren Nodes zu verteilen. So kann man eine Zahl oder einen String mittels `AtomicValue` zwischen Nodes konsistent verteilen. Das Raft-Protokoll eignet sich für eine Datenverteilung perfekt an, da es dafür vorgesehen ist.

#### Notifikation

* Atomix ist ein reines Cluster-Framework. Was man auf den einzelnen Nodes ausführt ist Atomix per se egal. Deshalb muss die Arbeitsaufteilung zwischen Workern selbst implementiert werden.  
* Atomix bietet aber die Möglichkeit eine Master-Slave Relation zu bilden, indem der Client immer seine Anfragen  (mittels Direct-Messaging) an den Master sendet und dieser eigenständig die Arbeit an Worker verteilt. Weiters besteht die Möglichkeit Anfragen mittels Event-Service an die Nodes mit Round-Robin zu verteilen. Dabei gibt es keinen Master, die Verteilung erfolgt über den Raft-Algorithmus.

### Akka

#### Architektur

* Aktoren Modell   
Akka ist eine der bekanntesten Implementierung eines Aktorenmodells für die JVM. Unter einem Aktorenmodell versteht man, dass nebenläufige Prozesse mittels Aktoren definiert werden. Die Aktoren sind lose gekoppelt und kommunizieren miteinander über asynchrone Nachrichten.
  
* Akka bietet die Möglichkeit mehre Akka-Applikationen als Cluster laufen zu lassen. Dabei Cluster besteht immer aus mehreren Nodes, wo bei manche Nodes als "Seed" Nodes konfiguriert werden. Seed Nodes unterscheiden sich nicht speziell von normalen Nodes, sie sind aber wichtig wenn neue Nodes dem Cluster joinen, denn diese wenden sich immer an die Seed Nodes, die das Hinzufügen übernehmen.

* Cluster auf Basis des Gossip Protokolls   
Bei Clustern auf Basis des Gossip-Protokolls (englisch für Tratsch-und-Klatsch) kommunizieren die Nodes mittels Peer-To-Peer zufällig miteinander.

#### Sprachen

* Java und somit alle anderen Sprachen, die auf der JVM basieren (Kotlin, Scala, .. ).
* Akka bietet mit Akka-http REST-Schnittstellen an, die man mit anderen Programmiersprachen aufrufen kann.

#### Datenverteilung

* Akka bietet mit der `Replicator`-API eine Möglichkeit Daten zwischen mehreren Nodes zu teilen. In Gegensatz zu Atomix ist die Implementierung deutlich aufwendiger, da keine vorgefertigten Typen vorhanden sind.

#### Notifikation

* Wie bei Atomix wird die Verteilung von Nachrichten an die Worker-Nodes durch den Cluster-Algorithmus, bei Akka Gossip, geregelt. 
* Jedoch legt Akka einen starken Fokus auf eine Master-Slave Relation auf den Worker-Nodes. Aufgaben sollen in kleinere Unter-Aufgaben zerstückelt werden, die durch mehrere Slaves parallel abgearbeitet werden sollen. Die Akka-API hilft bei der Implementierung eines solchen Vorhabens.

## Implementierung

Ich habe pro Framework zwei Benchmarks geschrieben, wobei ein Benchmark die Zeit misst, wie lange es braucht ein Cluster zu starten bzw. zu joinen und der Zweite, der die rohe Performance inklusive Kommunikationswege bewertet.

### Cluster Benchmark

Der Cluster-Benchmark besteht eigentlich aus zwei Teilen:

* Testlauf der die Zeit misst, wie lange das Cluster zu starten braucht
* Testlauf der die Zeit misst, wie lange ein Client braucht, dem Cluster zu joinen

Die Testläufe müssen genau in dieser Reihenfolge gestartet werden, den ein Client kann dem Cluster nur joinen, wenn er auch läuft.

#### Implementierung in Akka

Meine Implementierung basiert auf einem offiziellen [Example](https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats), welches ich umgebaut habe, um dieses mit Atomix vergleichbar zu machen.

Startet man den Benchmark mit dem Parameter `cluster`, werden drei Worker-Nodes mittels der `startup`-Method in der Klasse `App` erstellt. 

Beim ersten Aufruf dieser Methode wird dann die Zeitmessung für den Benchmark gestartet:

```java
private static void startup(String role, int port) {
        if (App.start == null) {
            System.out.println("Starting benchmark...");
            App.start = System.nanoTime();
        }
        ...
}
```

Die eigentliche Konfiguration geschieht dann in der `create`-Methode von der `RootBehavior`-Klasse. In dieser hat man durch den Akka-Kontext Zugriff auf das Cluster und seinen Status. 

Durch den Zugriff auf den Status kann man die Anzahl der Nodes zählen, falls diese drei beträgt, kann man sagen, dass das Cluster vollständig ist. Somit kann dieser Testlauf ausgegeben werden.

```java
private static class RootBehavior {
        static Behavior<Void> create() {
            return Behaviors.setup(context -> {
                Cluster cluster = Cluster.get(context.getSystem());
                ClusterEvent.CurrentClusterState state;

                if (cluster.selfMember().hasRole("compute")) {
                    boolean check;
                    do {
                        state = cluster.state();
                        check = state.members().size() == 3;
                        if (!check) {
                            Thread.sleep(10);
                        }
                    } while (!check);

                    synchronized (App.class) {
                        if (!App.logged) {
                            App.end = System.nanoTime();
                            App.runTime = App.end - App.start;

                            System.out.println("Cluster-Startup Benchmark done...");
                            System.out.println("Execution time in nanoseconds  : " + App.runTime);
                            System.out.println("Execution time in milliseconds : " + App.runTime / 1000000);
                            App.logged = true;
                        }
                    }
                    ...
```

Jetzt kann man den zweiten Testlauf starten, der schaut wie lange ein Client zum joinen braucht. Dieser funktioniert im Prinzip fast 1:1, wie der des Clusters. Für das Starten eines Clients wird auch die `startup`-Methode verwendet, jedoch mit der Rolle  `clientBenchmark`. 

In der `create`-Methode kann wieder mit einer Schleife geschaut werden, wie viele Member im Cluster sind. Die Anzahl muss vier ergeben, da wir ja drei Worker-Nodes haben und jetzt einen Client hinzufügen. 

```java
private static class RootBehavior {
        static Behavior<Void> create() {
            return Behaviors.setup(context -> {
                Cluster cluster = Cluster.get(context.getSystem());
                ClusterEvent.CurrentClusterState state;
				
				...
				
				if (cluster.selfMember().hasRole("clientBenchmark")) {
                    ActorRef<FibonacciService.ProcessFib> serviceRouter =
                            context.spawn(Routers.group(STATS_SERVICE_KEY), "ServiceRouter");
                    // Wait until clients join cluster
                    boolean check;
                    do {
                        state = cluster.state();
                        check = state.members().size() == 4;
                        if (!check) {
                            Thread.sleep(10);
                        }
                    } while (!check);

                    App.end = System.nanoTime();
                    App.runTime = App.end - App.start;

                    System.out.println("Client-Join Benchmark done...");
                    System.out.println("Execution time in nanoseconds  : " + App.runTime);
                    System.out.println("Execution time in milliseconds : " + App.runTime / 1000000);
                    System.exit(0);
                }
                ...
```



#### Implementierung in Atomix

Atomix ist in Vergleich zu Akka deutlich "schlanker" im Funktionsumfang. Während Akka Konfigurations-Files benötigt und die Logik in mehrere Aktoren unterteilt werden muss, reicht es in Atomix einfach eine Main-Klasse zu schreiben, die drei Worker-Nodes startet. Für den Benchmark muss man nur die Zeit zwischen der Erstellung und dem Starten der Nodes messen.

```java
public static void main(String[] args) throws InterruptedException {
        // Start benchmark
        Cluster.start = System.nanoTime();

        // Build node member
        AtomixBuilder builder0 = Atomix.builder();
        builder0.withMemberId("node0").withAddress("localhost:8080").build();
        // Attach node to cluster
        builder0.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                .withNodes(
                        Node.builder()
                                .withId("node0")
                                .withAddress("localhost:8080")
                                .build(),
                        Node.builder()
                                .withId("node1")
                                .withAddress("localhost:8081")
                                .build(),
                        Node.builder()
                                .withId("node2")
                                .withAddress("localhost:8082")
                                .build())
                .build());

        builder0.addProfile(Profile.dataGrid());
        Atomix node0 = builder0.build();
        // Start node
        node0.start().join();
        ...
        // Build and start node 1 and 2
        ...
        // Measure cluster startup-time
        Cluster.end = System.nanoTime();
        System.out.println("Cluster-Startup Benchmark done...");
        Cluster.runTime = Cluster.end - Cluster.start;
        System.out.println("Execution time in nanoseconds  : " + Cluster.runTime);
        System.out.println("Execution time in milliseconds : " + Cluster.runTime / 1000000);
}
```

Der Code, der misst wie lange es braucht, dass drei Clients dem Cluster joinen, sieht fast identisch aus, den Clients werden in Atomix auch als Nodes implementiert.



### Performance Benchmark

Der Performance-Benchmark misst die Zeit, wie lange es braucht, drei Client-Tasks vom Cluster abzuarbeiten und das Ergebnis an die Clients zurückzusenden. Als  einzelne Task fungiert die Berechnung der 9. Fibonacci-Folge.

#### Implementierung in Akka

Der Performance-Benchmark schließt im Grunde logisch an den Cluster-Benchmark an. Der Client wartet nun bis er dem Cluster gejoint ist und kreiert drei Aktoren, die die Tasks an das Cluster senden und auf Antworten warten.   

```java
private static class RootBehavior {
        static Behavior<Void> create() {
            return Behaviors.setup(context -> {
                Cluster cluster = Cluster.get(context.getSystem());

                ...

                if (cluster.selfMember().hasRole("clientBenchmark")) {
                    ActorRef<FibonacciService.ProcessFib> serviceRouter =
                            context.spawn(Routers.group(STATS_SERVICE_KEY), "ServiceRouter");
                    // Wait until clients join cluster
                    ClusterEvent.CurrentClusterState state;
                    boolean check;
                    do {
                        state = cluster.state();
                        check = state.members().size() == 4;
                        if (!check) {
                            Thread.sleep(10);
                        }
                    } while (!check);

                    // Wait for routes to register
                    Thread.sleep(2000);

                    // Spawn three test clients
                    context.spawn(FibonacciClientBenchmark.create(serviceRouter), "ClientBenchmark_0");
                    context.spawn(FibonacciClientBenchmark.create(serviceRouter), "ClientBenchmark_1");
                    context.spawn(FibonacciClientBenchmark.create(serviceRouter), "ClientBenchmark_2");

                }
```

Mit `context.spawn` werden konkret die Client-Aktoren gestartet. Davor habe ich aber ein noch ein `Thread.sleep(2000)` eingebaut. Wieso? Nun, wenn die Anzahl der Member auf vier ist, ist der Client dem Cluster gejoint, die Routen aber, die Ausschluss geben welcher Aktor wo im Cluster verfügbar ist, sind aber zu diesem Zeitpunkt noch nicht geladen. Wenn man sofort die Client-Aktoren ohne Verzögerung startet, kommen die Anfragen beim Cluster nie an:

![](images/akka_dead_letters.PNG)

Ich habe probiert, den Code schöner zu gestalten, ohne `sleep`. Jedoch habe ich keine Lösung dazu gefunden bzw. da ich die neue Cluster-Version von Akka verwende, fehlen einfach einige [API-Elemente](https://doc.akka.io/docs/akka/current/cluster-usage.html#subscribe-to-cluster-events), die geändert wurden. Wie diese Methode, die auf die erfolgreiche Registrierung im Cluster wartet:

```java
Cluster cluster = Cluster.get(getContext().getSystem());
  cluster.join(cluster.selfAddress());
cluster.registerOnMemberUp(
    () -> cluster.subscribe(getSelf(), MemberEvent.class, UnreachableMember.class));
```

Auch der [Lösungsvorschlag](https://github.com/akkadotnet/akka.net/issues/2327), die registrierten Routen zu zählen, funktionierte nicht, da das, soviel ich weiß, nur in der C#-Implementierung von Akka möglich ist.

Doch zurück zum Benchmark. Beim Starten der Benchmark-Clients wird in der `create`-Methode die Konfiguration vollzogen. Dabei wird auch die Task an das Cluster gesendet. Außerdem wird die Callback-Methode definiert, diese stoppt den Benchmark wenn drei Tasks erfolgreich abgeschlossen wurden.

```java
public static Behavior<Event> create(ActorRef<FibonacciService.ProcessFib> service) {
        return Behaviors.setup(context -> {

            ActorRef<FibonacciService.Response> responseAdapter =
                    context.messageAdapter(FibonacciService.Response.class, ServiceResponse::new);

            synchronized (FibonacciClientBenchmark.class) {
                if (FibonacciClientBenchmark.start == null) {
                    context.getLog().info("Starting benchmark...");
                    FibonacciClientBenchmark.start = System.nanoTime();
                }
            }

            // We want to make exactly of request
            context.getLog().info("Sending process request");
            service.tell(new FibonacciService.ProcessFib(46, responseAdapter));

            return Behaviors.receive(Event.class)
                .onMessage(ServiceResponse.class, response -> {
                    context.getLog().info("Service result: {}", response.result);
                    int done = FibonacciClientBenchmark.counter.incrementAndGet();
                    if (done == 3) {
                        FibonacciClientBenchmark.end = System.nanoTime();
                        context.getLog().info("Performance Benchmark done...");
                        FibonacciClientBenchmark.runTime =
                                FibonacciClientBenchmark.end - FibonacciClientBenchmark.start;

                        context.getLog().info("Execution time in nanoseconds  : " + FibonacciClientBenchmark.runTime);
                        context.getLog().info("Execution time in milliseconds : " +
                                FibonacciClientBenchmark.runTime / 1000000);
                        // Benchmark is done - stop it
                        System.exit(0);
                    }
                    // Actor is done - stop it
                    return Behaviors.stopped();
                }).build();
        });

    }
```

Wenn einer dieser Tasks bei einer Worker-Node ankommt, wird diese vom `FibonacciService` verarbeitet. Die Anfrage wird an den `FibonacciScheduler` weitergegeben, der den `FibonacciWorker` startet, der die eigentliche Berechnung durchführt. Der Scheduler ist normalerweise dazu da, eine Task weiter aufzuspalten und auf mehrere Worker intern aufzuteilen, da aber Atomix solche Komplexität nativ einfach nicht unterstützt, wird beim Benchmark hier die Task nicht weiter unterteilt und genau ein Worker gestartet.

Wichtig ist es beim Scheduler das Bearbeitungstimeout hoch zu stellen, wenn man lange Tasks, wie die Fibonacci-Berechnung durchführt. Ansonsten wird die Task immer vorzeitig unterbrochen und die Task schlägt fehlt.

```java
getContext().setReceiveTimeout(Duration.ofMinutes(5), Timeout.INSTANCE);
```



#### Implementierung in Atomix

Der Performance-Benchmark in Atomix ergänzt den Cluster-Benchmark um die Verwendung eines Event-Services. Beim Event-Service handelt es sich um eine Möglichkeit Aufgaben an Nodes zu verteilen. Ein Client kann eine Nachricht an den Cluster-Event-Service schicken, der per Round-Robin-Verfahren diese Nachricht einer Node zuteilt. Im Code wird jeder Worker-Node eine Callback-Funktion zugeteilt, die die Client-Task (die Fibonacci-Berechnung) bei Erhalt der Nachricht durchführt und das Ergebnis zurück an den Client schickt.

```java
// Add event listener to member
node0.getEventService().subscribe("test", message -> {
	long result = fibBenchmark(message);
    return CompletableFuture.completedFuture(result);
});
```

Clients können mittels `send` an den Event-Service eine Task senden, bei Erhalt eines Ergebnisses wird eine Callback-Funktion ausgeführt, die den Benchmark bei drei empfangenen Ergebnissen beendet. Dieser Benchmark sendet drei Anfragen zur Fibonacci-Berechnung per Event-Service an das Cluster.

```java
// Send a request-reply message via the event service
client0.getEventService().send("test", 46L, Duration.ofMinutes(5)).thenAccept(response -> {
	benchmark();
});
```

```java
private static void benchmark() {
        int done = Clients.counter.incrementAndGet();
        if (done == 3) {
            Clients.end = System.nanoTime();
            System.out.println("Performance Benchmark done...");
            Clients.runTime = Clients.end - Clients.start;
            System.out.println("Execution time in nanoseconds  : " + Clients.runTime);
            System.out.println("Execution time in milliseconds : " +
                    Clients.runTime / 1000000);
            // Benchmark is done - stop it
            System.exit(0);
        }
    }
```

Wie auch bei der Akka Implementierung benötigt Atomix eine kurze Verzögerung vor dem Senden. Eigentlich sollte der Code "blockieren" bis die Clients mittels `join`-Methode dem Cluster gejoint sind. Ohne Timeout jedoch kommen die Nachrichten nie beim Cluster an, eine Fehlermeldung wird nicht angezeigt.



## Benchmark-Ergebnisse

## Cluster-Benchmark

Beide Frameworks sind beim Initialisieren und Joinen eines Cluster vergleichbar. Akka startet ein bisschen langsamer, dafür joinen Clients schneller. Die Benchmark-Zeiten beinhalten jedoch nicht die Timeout-Zeiten, die ich setzen musste, um die Nachrichten an das Cluster fehlerfrei zu senden. Somit sollte man in Praxis eher fünf Sekunden zum Joinen erwarten.

### Akka

#### Cluster-Start

```
cd Akka-Demo
./gradlew cluster_cluster
```

![](images/cluster/akka_cluster_1.PNG)

![](images/cluster/akka_cluster_2.PNG)

![](images/cluster/akka_cluster_3.PNG)

#### Client-Join

```
cd Akka-Demo
./gradlew cluster_client
```

![](images/cluster/akka_client_1.PNG)

![](images/cluster/akka_client_2.PNG)

![](images/cluster/akka_client_3.PNG)



### Atomix

#### Cluster-Start

```
cd Atomix-Demo
./gradlew cluster_cluster
```

![](images/cluster/atomix_cluster_1.PNG)

![](images/cluster/atomix_cluster_2.PNG)

![](images/cluster/atomix_cluster_3.PNG)

#### Client-Join

```
cd Atomix-Demo
./gradlew cluster_client
```

![](images/cluster/atomix_client_1.PNG)

![](images/cluster/atomix_client_2.PNG)

![](images/cluster/atomix_client_3.PNG)





## Performance-Benchmark

Beim Performance-Benchmark führt Akka um ungefähr eine Sekunde. Daraus kann man schließen, dass die Kommunikation zwischen Clients und Clusters schneller funktioniert als in Atomix. Denn die Berechnung der Fibonacci-Zahlenfolge sollte fast identisch sein, da beide Sprachen auf der JVM basieren. Jedoch muss man beachten, dass der Garbage-Collector die Ergebnisse verfälschen kann.  Die schnellere Kommunikation kann man durch die verschiedenen Cluster-Algorithmen begründen, den das Raft-Protokoll was in Atomix eingesetzt wird, setzt verstärkt auf Konsistenz, während das von Akka benutzte Gossip-Protokoll auf eventuelle Konsistenz durch Peer-Peer setzt. Die strenge Einhaltung der Konsistenz kann sicher zu Verzögerungen führen.

### Akka

```
cd Akka-Demo
./gradlew performance_cluster
./gradlew performance_client
```

![](images/performance/akka_performance_1.PNG)

![](images/performance/akka_performance_2.PNG)

![](images/performance/akka_performance_3.PNG)

### Atomix

```
cd Atomix-Demo
./gradlew performance_cluster
./gradlew performance_client
```

![](images/performance/atomix_performance_1.PNG)

![](images/performance/atomix_performance_2.PNG)

![](images/performance/atomix_performance_3.PNG)

## Quellen

* [Atomix - Getting started | 25.02.2020](https://atomix.io/docs/latest/getting-started/)

* [Atomix - Publish-Subscribe Messaging | 25.02.2020](https://atomix.io/docs/latest/user-manual/cluster-communication/publish-subscribe-messaging/)

* [Atomix - API | 03.03.2020](https://atomix.io/docs/latest/api/)

* [Akka - Einführung | 25.02.2020](https://m.heise.de/developer/artikel/Einfuehrung-in-die-Aktor-Programmierung-mit-Akka-1921580.html?seite=all)

* [Why Akka Cluster? | 25.02.2020](https://blog.softwaremill.com/when-do-you-need-akka-cluster-5885d43e901b)

* [Raft Algorithmus | 25.02.2020](https://www.geeksforgeeks.org/raft-consensus-algorithm/)

* [Akka Cluster Example | 01.03.2020](https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java)

* [Measure Time in Java | 02.02.2020](https://www.techiedelight.com/measure-elapsed-time-execution-time-java/)

* [Akka Cluster Concept](https://doc.akka.io/docs/akka/current/typed/cluster-concepts.html)

  