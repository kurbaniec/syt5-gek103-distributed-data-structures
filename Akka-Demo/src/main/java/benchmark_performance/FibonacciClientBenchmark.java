package benchmark_performance;


import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.Behaviors;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public final class FibonacciClientBenchmark {
    // Benchmark relevant variables
    public static Long start;
    public static Long end;
    public static Long runTime;
    public static AtomicInteger counter = new AtomicInteger();

    interface Event {}
    private enum Tick implements Event {
        INSTANCE
    }
    private static class ServiceResponse implements Event {
        public final FibonacciService.Response result;
        public ServiceResponse(FibonacciService.Response result) {
            this.result = result;
        }
    }

    /**
     * Behavior:
     * The behavior of an actor defines how it reacts to the messages that it
     * receives.
     * ActorRef:
     * An ActorRef is the identity or address of an Actor instance.
     */
    public static Behavior<Event> create(ActorRef<FibonacciService.ProcessFib> service) {
        return Behaviors.setup(context -> {

            ActorRef<FibonacciService.Response> responseAdapter =
                    context.messageAdapter(FibonacciService.Response.class, ServiceResponse::new);

            synchronized (FibonacciClientBenchmark.class) {
                if (FibonacciClientBenchmark.start == null) {
                    context.getLog().info("Starting benchmark...");
                    FibonacciClientBenchmark.start = System.nanoTime();
                }
            }

            // We want to make exactly of request
            context.getLog().info("Sending process request");
            service.tell(new FibonacciService.ProcessFib(46, responseAdapter));

            return Behaviors.receive(Event.class)
                .onMessage(ServiceResponse.class, response -> {
                    context.getLog().info("Service result: {}", response.result);
                    int done = FibonacciClientBenchmark.counter.incrementAndGet();
                    if (done == 3) {
                        FibonacciClientBenchmark.end = System.nanoTime();
                        context.getLog().info("Performance Benchmark done...");
                        FibonacciClientBenchmark.runTime =
                                FibonacciClientBenchmark.end - FibonacciClientBenchmark.start;

                        context.getLog().info("Execution time in nanoseconds  : " + FibonacciClientBenchmark.runTime);
                        context.getLog().info("Execution time in milliseconds : " +
                                FibonacciClientBenchmark.runTime / 1000000);
                        // Benchmark is done - stop it
                        System.exit(0);
                    }
                    // Actor is done - stop it
                    return Behaviors.stopped();
                }).build();
        });

    }

}