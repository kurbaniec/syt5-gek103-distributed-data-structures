package benchmark_performance;


import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.time.Duration;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public class FibonacciScheduler extends AbstractBehavior<FibonacciScheduler.Event> {

    interface Event {}
    private enum Timeout implements Event {
        INSTANCE
    }
    private static class CalculationComplete implements Event {
        public final long result;
        public CalculationComplete(long result) {
            this.result = result;
        }
    }

    public static Behavior<Event> create(long n, ActorRef<FibonacciWorker.Process> workers, ActorRef<FibonacciService.Response> replyTo) {
        return Behaviors.setup(context ->
                new FibonacciScheduler(context, n, workers, replyTo)
        );
    }

    private final ActorRef<FibonacciService.Response> replyTo;

    private FibonacciScheduler(ActorContext<Event> context, long n, ActorRef<FibonacciWorker.Process> workers, ActorRef<FibonacciService.Response> replyTo) {
        super(context);
        this.replyTo = replyTo;
        // Add extra long timeout
        getContext().setReceiveTimeout(Duration.ofMinutes(5), Timeout.INSTANCE);

        ActorRef<FibonacciWorker.Processed> responseAdapter =
                getContext().messageAdapter(FibonacciWorker.Processed.class, processed -> new CalculationComplete(processed.result));

        workers.tell(new FibonacciWorker.Process(n, responseAdapter));
    }

    @Override
    public Receive<Event> createReceive() {
        return newReceiveBuilder()
                .onMessage(CalculationComplete.class, this::onCalculationComplete)
                .onMessageEquals(Timeout.INSTANCE, this::onTimeout)
                .build();
    }

    private Behavior<Event> onCalculationComplete(CalculationComplete event) {
        replyTo.tell(new FibonacciService.JobResult(event.result));
        return Behaviors.stopped();
    }

    private Behavior<Event> onTimeout() {
        replyTo.tell(new FibonacciService.JobFailed("Service unavailable, try again later"));
        return Behaviors.stopped();
    }
}