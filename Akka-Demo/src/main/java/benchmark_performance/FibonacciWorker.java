package benchmark_performance;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import serialize.CborSerializable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public final class FibonacciWorker extends AbstractBehavior<FibonacciWorker.Command> {

    interface Command extends CborSerializable {}
    public static final class Process implements Command {
        public final long n;
        public final ActorRef<Processed> replyTo;
        public Process(long n, ActorRef<Processed> replyTo) {
            this.n = n;
            this.replyTo = replyTo;
        }
    }
    private enum EvictCache implements Command {
        INSTANCE
    }
    public static final class Processed implements CborSerializable {
        public final long n;
        public final long result;
        public Processed(long n, long result) {
            this.n = n;
            this.result = result;
        }
    }

    private final Map<Long, Long> cache = new HashMap<>();

    private FibonacciWorker(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<FibonacciWorker.Command> create() {
        return Behaviors.setup(context ->
                Behaviors.withTimers(timers -> {
                    context.getLog().info("Worker starting up");
                    // @Note This timer messes up startup-time and leads to dead messages error
                    // timers.startTimerWithFixedDelay(EvictCache.INSTANCE, EvictCache.INSTANCE, Duration.ofSeconds(30));
                    return new FibonacciWorker(context);
                })
        );
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(Process.class, this::process)
                .onMessageEquals(EvictCache.INSTANCE, this::evictCache)
                .build();
    }

    private Behavior<Command> evictCache() {
        cache.clear();
        return this;
    }

    private Behavior<Command> process(Process command) {
        getContext().getLog().info("Worker processing request");
        if (!cache.containsKey(command.n)) {
            System.out.println("N: " + command.n);
            long result = fib(command.n);
            cache.put(command.n, result);
        }
        command.replyTo.tell(new Processed(command.n, cache.get(command.n)));
        return this;
    }

    private long fib(long n) {
        if (n <= 1)
            return n;
        return fib(n-1) + fib(n-2);
    }
}