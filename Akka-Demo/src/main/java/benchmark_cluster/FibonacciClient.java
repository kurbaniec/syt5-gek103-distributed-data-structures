package benchmark_cluster;


import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.Behaviors;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public final class FibonacciClient {

    interface Event {}
    private enum Tick implements Event {
        INSTANCE
    }
    private static class ServiceResponse implements Event {
        public final FibonacciService.Response result;
        public ServiceResponse(FibonacciService.Response result) {
            this.result = result;
        }
    }

    /**
     * Behavior:
     * The behavior of an actor defines how it reacts to the messages that it
     * receives.
     * ActorRef:
     * An ActorRef is the identity or address of an Actor instance.
     */
    public static Behavior<Event> create(ActorRef<FibonacciService.ProcessFib> service) {
         return Behaviors.setup(context ->
            Behaviors.withTimers(timers -> {

                ActorRef<FibonacciService.Response> responseAdapter =
                    context.messageAdapter(FibonacciService.Response.class, ServiceResponse::new);

                return Behaviors.receive(Event.class)
                    .onMessageEquals(Tick.INSTANCE, () -> {
                        context.getLog().info("Sending process request");
                        service.tell(new FibonacciService.ProcessFib(9, responseAdapter));
                        return Behaviors.same();
                    }).onMessage(ServiceResponse.class, response -> {
                        context.getLog().info("Service result: {}", response.result);
                        // Actor is done - stop it
                        System.exit(0);
                        return Behaviors.stopped();
                    }).build();
              })
          );

    }

}