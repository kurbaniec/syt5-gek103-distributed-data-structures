package benchmark_cluster;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.fasterxml.jackson.annotation.JsonCreator;
import serialize.CborSerializable;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public final class FibonacciService extends AbstractBehavior<FibonacciService.Command> {

    public interface Command extends CborSerializable {}
    public final static class ProcessFib implements Command {
        public final int n;
        public final ActorRef<Response> replyTo;
        public ProcessFib(int n, ActorRef<Response> replyTo) {
            this.n = n;
            this.replyTo = replyTo;
        }
    }
    public enum Stop implements Command {
        INSTANCE
    }

    interface Response extends CborSerializable { }
    public static final class JobResult implements Response {
        public final long fibonacciSequence;
        @JsonCreator
        public JobResult(long fibonacciSequence) {
            this.fibonacciSequence = fibonacciSequence;
        }
        @Override
        public String toString() {
            return String.valueOf(fibonacciSequence);
        }
    }
    public static final class JobFailed implements Response {
        public final String reason;
        @JsonCreator
        public JobFailed(String reason) {
            this.reason = reason;
        }
        @Override
        public String toString() {
            return "JobFailed{" +
                    "reason='" + reason + '\'' +
                    '}';
        }
    }

    public static Behavior<Command> create(ActorRef<FibonacciWorker.Process> workers) {
        return Behaviors.setup(context ->
                new FibonacciService(context, workers)
        );
    }

    private final ActorRef<FibonacciWorker.Process> workers;

    private FibonacciService(ActorContext<Command> context, ActorRef<FibonacciWorker.Process> workers) {
        super(context);
        this.workers = workers;
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(ProcessFib.class, this::process)
                .onMessageEquals(Stop.INSTANCE, () -> Behaviors.stopped())
                .build();
    }

    private Behavior<Command> process(ProcessFib command) {
        getContext().spawnAnonymous(FibonacciScheduler.create(command.n, workers, command.replyTo));
        return this;
    }

}