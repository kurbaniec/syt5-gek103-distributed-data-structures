package benchmark_cluster;

import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Routers;
import akka.actor.typed.receptionist.Receptionist;
import akka.actor.typed.receptionist.ServiceKey;
import akka.cluster.ClusterEvent;
import akka.cluster.typed.Cluster;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Akka
 * Source: https://github.com/akka/akka-samples/tree/2.6/akka-sample-cluster-java/src/main/java/sample/cluster/stats
 */
public class App {

    public static Long start;
    public static Long end;
    public static Long runTime;
    public static boolean logged = false;

    static final ServiceKey<FibonacciService.ProcessFib> STATS_SERVICE_KEY =
            ServiceKey.create(FibonacciService.ProcessFib.class, "StatsService");

    private static class RootBehavior {
        static Behavior<Void> create() {
            return Behaviors.setup(context -> {
                Cluster cluster = Cluster.get(context.getSystem());
                ClusterEvent.CurrentClusterState state;

                if (cluster.selfMember().hasRole("compute")) {
                    boolean check;
                    do {
                        state = cluster.state();
                        check = state.members().size() == 3;
                        if (!check) {
                            Thread.sleep(10);
                        }
                    } while (!check);

                    synchronized (App.class) {
                        if (!App.logged) {
                            App.end = System.nanoTime();
                            App.runTime = App.end - App.start;

                            System.out.println("Cluster-Startup Benchmark done...");
                            System.out.println("Execution time in nanoseconds  : " + App.runTime);
                            System.out.println("Execution time in milliseconds : " + App.runTime / 1000000);
                            App.logged = true;
                        }
                    }

                    // on every compute node there is one service instance that delegates to N local workers
                    final int numberOfWorkers = context.getSystem().settings().config().getInt("stats-service.workers-per-node");
                    ActorRef<FibonacciWorker.Command> workers =
                            context.spawn(Routers.pool(numberOfWorkers, FibonacciWorker.create()), "WorkerRouter");
                    ActorRef<FibonacciService.Command> service =
                            context.spawn(FibonacciService.create(workers.narrow()), "StatsService");

                    // published through the receptionist to the other nodes in the cluster
                    context.getSystem().receptionist().tell(Receptionist.register(STATS_SERVICE_KEY, service.narrow()));

                }
                if (cluster.selfMember().hasRole("client")) {
                    ActorRef<FibonacciService.ProcessFib> serviceRouter =
                            context.spawn(Routers.group(STATS_SERVICE_KEY), "ServiceRouter");
                    context.spawn(FibonacciClient.create(serviceRouter), "Client");
                }

                if (cluster.selfMember().hasRole("clientBenchmark")) {
                    ActorRef<FibonacciService.ProcessFib> serviceRouter =
                            context.spawn(Routers.group(STATS_SERVICE_KEY), "ServiceRouter");
                    // Wait until clients join cluster
                    boolean check;
                    do {
                        state = cluster.state();
                        check = state.members().size() == 4;
                        if (!check) {
                            Thread.sleep(10);
                        }
                    } while (!check);

                    App.end = System.nanoTime();
                    App.runTime = App.end - App.start;

                    System.out.println("Client-Join Benchmark done...");
                    System.out.println("Execution time in nanoseconds  : " + App.runTime);
                    System.out.println("Execution time in milliseconds : " + App.runTime / 1000000);
                    System.exit(0);
                }

                return Behaviors.empty();
            });
        }
    }

    public static void main(String[] args) {

        if (args.length == 1) {
            if (args[0].equals("cluster")) {
                startup("compute", 25251);
                startup("compute", 25252);
                startup("compute", 0);
            } else if (args[0].equals("client")) {
                startup("clientBenchmark", 0);
            } else {
                throw new IllegalArgumentException(args[0] +
                    " is not a valid benchmark method! Use 'cluster' or 'client'");
            }
        } else if (args.length == 0) {
            startup("compute", 25251);
            startup("compute", 25252);
            startup("compute", 0);
            startup("client", 0);
        } else {
            if (args.length != 2)
                throw new IllegalArgumentException("Usage: role port");
            startup(args[0], Integer.parseInt(args[1]));
        }
    }

    private static void startup(String role, int port) {
        if (App.start == null) {
            System.out.println("Starting benchmark...");
            App.start = System.nanoTime();
        }
        // Override the configuration of the port
        Map<String, Object> overrides = new HashMap<>();
        overrides.put("akka.remote.artery.canonical.port", port);
        overrides.put("akka.cluster.roles", Collections.singletonList(role));

        Config config = ConfigFactory.parseMap(overrides)
                .withFallback(ConfigFactory.load("fib"));

        ActorSystem<Void> system = ActorSystem.create(RootBehavior.create(), "ClusterSystem", config);
    }
}