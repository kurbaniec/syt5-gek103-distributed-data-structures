package benchmark_performance;

import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.profile.Profile;

import java.util.concurrent.CompletableFuture;

/**
 * Starts a Cluster for benchmarking.
 * @author Kacper Urbaniec
 * @version 2020-02-25
 */
public class Cluster {
    public static void main(String[] args) throws InterruptedException {

        // Build node member
        AtomixBuilder builder0 = Atomix.builder();
        builder0.withMemberId("node0").withAddress("localhost:8080").build();
        // Attach node to cluster
        builder0.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                .withNodes(
                        Node.builder()
                                .withId("node0")
                                .withAddress("localhost:8080")
                                .build(),
                        Node.builder()
                                .withId("node1")
                                .withAddress("localhost:8081")
                                .build(),
                        Node.builder()
                                .withId("node2")
                                .withAddress("localhost:8082")
                                .build())
                .build());

        builder0.addProfile(Profile.dataGrid());
        Atomix node0 = builder0.build();
        // Add event listener to member
        node0.getEventService().subscribe("test", message -> {
            long result = fibBenchmark(message);
            return CompletableFuture.completedFuture(result);
        });

        // Start node
        node0.start().join();


        // Build node member
        AtomixBuilder builder1 = Atomix.builder();
        builder1.withMemberId("node1").withAddress("localhost:8081").build();
        // Attach node to cluster
        builder1.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                .withNodes(
                        Node.builder()
                                .withId("node0")
                                .withAddress("localhost:8080")
                                .build(),
                        Node.builder()
                                .withId("node1")
                                .withAddress("localhost:8081")
                                .build(),
                        Node.builder()
                                .withId("node2")
                                .withAddress("localhost:8082")
                                .build())
                .build());

        builder1.addProfile(Profile.dataGrid());
        Atomix node1 = builder1.build();
        // Add event listener to member
        node1.getEventService().subscribe("test", message -> {
            long result = fibBenchmark(message);
            return CompletableFuture.completedFuture(result);
        });

        // Start node
        node1.start().join();


        // Build node member
        AtomixBuilder builder2 = Atomix.builder();
        builder2.withMemberId("node2").withAddress("localhost:8082").build();
        // Attach node to cluster
        builder2.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                .withNodes(
                        Node.builder()
                                .withId("node0")
                                .withAddress("localhost:8080")
                                .build(),
                        Node.builder()
                                .withId("node1")
                                .withAddress("localhost:8081")
                                .build(),
                        Node.builder()
                                .withId("node2")
                                .withAddress("localhost:8082")
                                .build())
                .build());

        builder2.addProfile(Profile.dataGrid());
        Atomix node2 = builder2.build();
        // Add event listener to member
        node2.getEventService().subscribe("test", message -> {
            long result = fibBenchmark(message);
            return CompletableFuture.completedFuture(result);
        });

        // Start node
        node2.start().join();
    }

    private static long fibBenchmark(Object message) {
        long n = 46L;
        if (message instanceof Long) {
            n = (long) message;
        }
        System.out.println("N" + n);
        long result = fib(n);
        System.out.println("Result: " + result);
        return result;
    }

    private static long fib(long n) {
        //System.out.println("calculating");
        if (n <= 1)
            return n;
        return fib(n-1) + fib(n-2);
    }


}
