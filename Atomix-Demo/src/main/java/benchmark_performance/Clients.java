package benchmark_performance;

import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Benchmarks the cluster through Fibonacci.
 * @author Kacper Urbaniec
 * @version 2020-03-03
 */
public class Clients {
    // Benchmark relevant variables
    public static Long start;
    public static Long end;
    public static Long runTime;
    public static AtomicInteger counter = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        // Create client
        AtomixBuilder clientBuilder0 = Atomix.builder()
                .withMemberId("client0")
                .withAddress("localhost:9080")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client0 = clientBuilder0.build();
        // Start client
        client0.start().join();


        // Create client
        AtomixBuilder clientBuilder1 = Atomix.builder()
                .withMemberId("client1")
                .withAddress("localhost:9081")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client1 = clientBuilder1.build();
        // Start client
        client1.start().join();


        // Create client
        AtomixBuilder clientBuilder2 = Atomix.builder()
                .withMemberId("client2")
                .withAddress("localhost:9082")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client2 = clientBuilder2.build();
        // Start client
        client2.start().join();


        // Wait for join
        Thread.sleep(2000);

        // Starting benchmark
        Clients.start = System.nanoTime();

        // Send a request-reply message via the event service
        client0.getEventService().send("test", 46L, Duration.ofMinutes(5)).thenAccept(response -> {
            benchmark();
        });

        // Send a request-reply message via the event service
        client1.getEventService().send("test", 46L, Duration.ofMinutes(5)).thenAccept(response -> {
            benchmark();
        });

        // Send a request-reply message via the event service
        client2.getEventService().send("test", 46L, Duration.ofMinutes(5)).thenAccept(response -> {
            benchmark();
        });

    }

    private static void benchmark() {
        int done = Clients.counter.incrementAndGet();
        if (done == 3) {
            Clients.end = System.nanoTime();
            System.out.println("Performance Benchmark done...");
            Clients.runTime = Clients.end - Clients.start;
            System.out.println("Execution time in nanoseconds  : " + Clients.runTime);
            System.out.println("Execution time in milliseconds : " +
                    Clients.runTime / 1000000);
            // Benchmark is done - stop it
            System.exit(0);
        }
    }
}
