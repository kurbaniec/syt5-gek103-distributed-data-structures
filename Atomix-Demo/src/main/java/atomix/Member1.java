package atomix;

import io.atomix.cluster.MemberId;
import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;
import io.atomix.core.profile.Profile;

import java.util.concurrent.CompletableFuture;

/**
 * @author Kacper Urbaniec
 * @version 2020-02-25
 */
public class Member1 {
    public static void main(String[] args) throws InterruptedException {

        // Build node member
        AtomixBuilder builder = Atomix.builder();
        builder.withMemberId("member1").withAddress("localhost:8080").build();
        // Attach node to cluster
        builder.withMembershipProvider(BootstrapDiscoveryProvider.builder()
                .withNodes(
                        Node.builder()
                                .withId("member1")
                                .withAddress("localhost:8080")
                                .build(),
                        Node.builder()
                                .withId("member2")
                                .withAddress("localhost:8081")
                                .build())
                .build());

        builder.addProfile(Profile.dataGrid());
        Atomix member1 = builder.build();
        // Add listener to member
        member1.getCommunicationService().subscribe("Test", message -> {
            System.out.println("Houston1!");
            return CompletableFuture.completedFuture(message);
        });
        // Add event listener to member
        member1.getEventService().subscribe("test", message -> {
            String respone = "Member1";
            return CompletableFuture.completedFuture(respone);
        });

        // Start node
        member1.start().join();

        // Create client
        AtomixBuilder clientBuilder = Atomix.builder()
                .withMemberId("client1")
                .withAddress("localhost:9080")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("member1")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("member2")
                                        .withAddress("localhost:8081")
                                        .build())
                        .build());

        Atomix client = clientBuilder.build();
        // Start client
        client.start().join();

        /**
        // Send message to member1 node
        client
            .getCommunicationService()
            .send("Test", "baum", MemberId.from("member1"));
        // Send another message to member1 node
        Thread.sleep(2000);
        client
                .getCommunicationService()
                .send("Test", "baum", MemberId.from("member1"));

        // Send a request-reply message via the event service
         */

        while (true) {
            client.getEventService().send("test", "Hello world!").thenAccept(response -> {
                System.out.println("Received " + response);
            });
            Thread.sleep(2000);
        }
    }


}
