package benchmark_cluster;

import io.atomix.cluster.Node;
import io.atomix.cluster.discovery.BootstrapDiscoveryProvider;
import io.atomix.core.Atomix;
import io.atomix.core.AtomixBuilder;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Benchmarks the cluster through Fibonacci.
 * @author Kacper Urbaniec
 * @version 2020-03-03
 */
public class Clients {
    // Benchmark relevant variables
    public static Long start;
    public static Long end;
    public static Long runTime;
    public static AtomicInteger counter = new AtomicInteger();

    public static void main(String[] args) throws InterruptedException {
        // Start benchmark
        Clients.start = System.nanoTime();

        // Create client
        AtomixBuilder clientBuilder0 = Atomix.builder()
                .withMemberId("client0")
                .withAddress("localhost:9080")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client0 = clientBuilder0.build();
        // Start client
        client0.start().join();


        // Create client
        AtomixBuilder clientBuilder1 = Atomix.builder()
                .withMemberId("client1")
                .withAddress("localhost:9081")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client1 = clientBuilder1.build();
        // Start client
        client1.start().join();


        // Create client
        AtomixBuilder clientBuilder2 = Atomix.builder()
                .withMemberId("client2")
                .withAddress("localhost:9082")
                .withMembershipProvider(BootstrapDiscoveryProvider.builder()
                        .withNodes(
                                Node.builder()
                                        .withId("node0")
                                        .withAddress("localhost:8080")
                                        .build(),
                                Node.builder()
                                        .withId("node1")
                                        .withAddress("localhost:8081")
                                        .build(),
                                Node.builder()
                                        .withId("node2")
                                        .withAddress("localhost:8082")
                                        .build())
                        .build());

        Atomix client2 = clientBuilder2.build();
        // Start client
        client2.start().join();

        // Measure cluster startup-time
        Clients.end = System.nanoTime();
        System.out.println("Client-Join Benchmark done...");
        Clients.runTime = Clients.end - Clients.start;
        System.out.println("Execution time in nanoseconds  : " + Clients.runTime);
        System.out.println("Execution time in milliseconds : " + Clients.runTime / 1000000);

    }
}
